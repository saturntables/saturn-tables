=== Saturn Tables ===
Contributors: saturntables
Donate link: https://saturntables.com/
Tags: data tables, list tables, data sources
Requires at least: 5.0
Tested up to: 5.6
Stable tag: 1.2.4
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A List Table creation plugin to enable datasources and data tables to be displayed in the WordPress administration panel in the native dashboard tabular format.

== Description ==

Saturn Tables provides a construct for methodical and organized construction of WordPress List Tables, the native format WordPress uses to display tabular data in the administration area. Designed for developers, this plugin is usually for projects involving data outside the WordPress data structure.

With simple PHP and MySQL programming, this plugin will construct beautiful and seamless data tables for custom datasets in the administration area of WordPress. And it is much easier than constructing custom data tables, pagination and other functionality from scratch. Little HTML or styling is needed, the tables appear just like the WordPress Posts or Pages list tables.

This plugin is new in 2021, feel free to reach out with questions.

== Frequently Asked Questions ==

= How does Saturn Tables work? =

Saturn Tables works by using a hook and callback system built on top of the construct WordPress has for custom administration menus.

= What level of WordPress knowledge is needed? =

If you can set up a custom WordPress menu or a simple plugin it would be helpful. There is a assumption you would know some MySQL since Saturn Tables is for displaying data in the administration area. WordPress style PHP, hooks and callbacks would be helpful.

= How do I get started? =

It would probably be easiest to install the sample code and data provided on the [website](https://saturntables.com "Saturn Tables") and then fork your project from there. Two sample code bases are provided for use and demonstration, you should be able to morph from there.

= Anything else I should know =

Saturn Tables has no settings or administration pages , it works by putting code, PHP files, in a subdirectory called /saturn-tables/ of your theme or child theme. You could also put the code in functions.php, but it is easier to organize in a separate file.

== Changelog ==

= 1.2.4 =
* Small bug fix.

= 1.2.3 =
* Initial Release.


