=== Saturn Tables ===
Contributors: saturntables
Tags: List Tables, Data
Donate link: https://saturntables.com/
Requires at least: 5.0
Tested up to: 5.5.3
Requires PHP: 5.6
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A List Table creation plugin to enable native and external datasources and datatables to be displayed in the Wordpress admin panel in native dashboard display format.